from invoke import Collection, ctask as task
from invoke.util import log

@task
def install_gems_clis(ctx):
    """ Install required gems """
    pass

@task
def install_gem_clis(ctx):
    """ Install required gems """
    pass

@task
def scour(ctx, filename):
    """Clean svg files"""
    ctx.run("scour -i {0} -o {0}_opt.svg --enable-viewboxing --enable-comment-stripping --shorten-ids --indent=none".format(filename))

@task
def lint(ctx):
    """lint - check style with flake8."""
    run('flake8 {{ cookiecutter.repo_name|replace('-', '_') }} tests')

@task
def clean(ctx):
    ctx.run("compass clean")
    ctx.run("rm -Rf {{ cookiecutter.repo_name|replace('-', '_') }}/compass/.sass-cache")

    log.info('cleaned up')

@task
def scss(ctx):
    ctx.run("compass compile --debug-info --time --trace -c {{ cookiecutter.repo_name|replace('-', '_') }}/compass/config.rb compass/")

    log.info('compass compiled')

@task
def assets(ctx):
    ctx.run("python {{ cookiecutter.repo_name|replace('-', '_') }}/manage.py collectstatic --noinput")

    log.info('Assets collected')

@task
def prun(ctx):
    ctx.run("python {{ cookiecutter.repo_name|replace('-', '_') }}/manage.py runserver 0.0.0.0:8000 --traceback")

@task(pre=[scss, assets, prun])
def rall(ctx):
    log.info('rall: All done')

dj = Collection("dj", clean, scss, assets, prun, rall)
dj.configure({"run":{"echo": True, "pty": True}})
dj.name = "dj"
