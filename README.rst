cookiecutter-django
=======================

The holydog cookiecutter_ template for Django.

.. _cookiecutter: https://github.com/audreyr/cookiecutter


Features
---------

* For Django 1.7
* Twitter Bootstrap_ 3
* AngularJS_
* Settings management via django-configurations_
* Registration via django-allauth_
* User avatars via django-avatar_
* Basic caching setup
* Grunt build for compass and livereload
* Basic e-mail configurations for send emails via SendGrid_

.. _Bootstrap: https://github.com/twbs/bootstrap
.. _AngularJS: https://github.com/angular/angular.js
.. _django-configurations: https://github.com/jezdez/django-configurations
.. _django-allauth: https://github.com/pennersr/django-allauth

Constraints
-----------

* Only maintained 3rd party libraries are used.
* PostgreSQL everywhere
* Environment variables for configuration (This won't work with Apache/mod_wsgi).


Usage
------

Let's pretend you want to create a Django project called "redditclone". Rather than using `startproject`
and then editing the results to include your name, email, and various configuration issues that always get forgotten until the worst possible moment, get cookiecutter_ to do all the work.

First, get cookiecutter. Trust me, it's awesome::

    $ pip install cookiecutter

Now run it against this repo::

    $ cookiecutter https://github.com/pydanny/cookiecutter-django.git
    $ cookiecutter https://bitbucket.org/slothyrulez/cookiecutter-holydog-django.git

You'll be prompted for some questions, answer them, then it will create a Django project for you.


**Warning**: After this point, change 'Daniel Greenfeld', 'pydanny', etc to your own information.

It prompts you for questions. Answer them::

    Cloning into 'cookiecutter-django'...
    remote: Counting objects: 550, done.
    remote: Compressing objects: 100% (310/310), done.
    remote: Total 550 (delta 283), reused 479 (delta 222)
    Receiving objects: 100% (550/550), 127.66 KiB | 58 KiB/s, done.
    Resolving deltas: 100% (283/283), done.
    project_name (default is "project_name is the title of the project.")? Reddit Clone
    repo_name (default is "reddit_clone")? reddit
    author_name (default is "Your Name")? Daniel Greenfeld
    email (default is "Your email")? pydanny@gmail.com
    description (default is "A short description of the project.")? A reddit clone.
    domain_name (default is "example.com")? myreddit.com
    version (default is "0.1.0")? 0.0.1
    now (default is "2015/01/13")? 2015/01/16
    year (default is "2015")?


Enter the project and take a look around::

    $ cd redditclone/
    $ ls

Create a GitHub repo and push it there::

    $ git init
    $ git add .
    $ git commit -m "first awesome commit"
    $ git remote add origin git@github.com:pydanny/redditclone.git
    $ git push -u origin master

Now take a look at your repo. Don't forget to carefully look at the generated README. Awesome, right?

Getting up and running
----------------------

The steps below will get you up and running with a local development environment. We assume you have the following installed:

* pip
* virtualenv
* PostgreSQL

First make sure to create and activate a virtualenv_, then open a terminal at the project root and install the requirements for local development::

    $ pip install -r requirements/local.txt

.. _virtualenv: http://docs.python-guide.org/en/latest/dev/virtualenvs/

Then, create a PostgreSQL database and add the database configuration using the  ``dj-database-url`` app pattern: ``postgres://db_owner:password@dbserver_ip:port/db_name`` either:

* in the ``config.common.py`` setting file,
* or in the env variable ``DATABASE_URL``



You can now run the usual Django ``migrate`` and ``runserver`` command (replace ``yourapp`` with the name of the directory containing the Django project)::

    $ python yourapp/manage.py migrate

    $ python yourapp/manage.py runserver

The base app will run but you'll need to carry out a few steps to make the sign-up and login forms work. These are currently detailed in `issue #39`_.

.. _issue #39: https://github.com/pydanny/cookiecutter-django/issues/39



TODO
----

ASSETS DOWNLOAD
~~~~~~~~~~~~~~~
On creation, common assets download

* jQuery
* Modernizer

GOOGLE TOOLS
~~~~~~~~~~~~
App to include google analytics

