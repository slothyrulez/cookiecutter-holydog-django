# -*- coding: utf-8 -*-

import sys
import os
import getpass
import cookiecutter

# - INSTALL REQUIREMENTS
# - CREATE PG DATABASE

## INSTALL REQUIREMENTS
import pip

ROOT = os.path.abspath('{{ cookiecutter.repo_name }}')
BASE_REQUIREMENTS = os.path.join(ROOT, "requirements", "local.txt")

# Actully we are on the recently root of the created project
pip.main(['install', '--upgrade', '-r', BASE_REQUIREMENTS])


## NOW WE HAVE REQUIREMENTS
## CREATE PG LOCAL DATABASE
from psycopg2 import connect, OperationalError
from psycopg2.extensions import ISOLATION_LEVEL_AUTOCOMMIT

PG_CONNECTION_DATA = {
    'dbname': 'postgres',
    'user': 'postgres',
    'host': 'localhost',
}


PG_CONNECTION_DATA["password"] = getpass.getpass("Insert pg postgres pasword: ")
# PG_CONNECTION_DATA["password"] = cookiecutter.compat.read_response()

try:
    pgcon = connect(**PG_CONNECTION_DATA)
except OperationalError as e:
    import ipdb
    ipdb.set_trace()

    print("ERROR {}".format(e.message))
pgcon.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT)
dbname = '{{ cookiecutter.repo_name }}'
dbusername = '{{ cookiecutter.dbusername }}'
dbpassword = '{{ cookiecutter.randomdbpassword }}'
